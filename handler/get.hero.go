package handler

import (
	"encoding/json"
	"net/http"
	"strconv"
	"strings"

	"github.com/jeffotoni/quick"
	e "gitlab.com/jeffotoni1/eventogo/sdk/error"
	"gitlab.com/jeffotoni1/eventogo/sdk/fastdata/zerohero/connect"
)

type Env connect.Env

func (env Env) GetHero(c *quick.Ctx) error {
	c.Set("Engine", "Quick")
	c.Set("Content-Type", "application/json")

	protocol := strings.ToLower(c.Param("protocol"))
	if protocol == "" {
		return c.Status(http.StatusBadRequest).JSON(e.NewError("URL protocol not provided."))
	}

	param := strings.ToLower(c.Param("param"))
	if param == "" {
		return c.Status(http.StatusBadRequest).JSON(e.NewError("URL param not provided."))
	}
	if protocol != "http" && protocol != "grpc" {
		return c.Status(http.StatusBadRequest).JSON(e.NewError("protocol not provided [http/grpc]."))
	}

	// var remoteAddr string
	// r := c.Request
	// if xff := r.Header.Get("X-Forwarded-For"); xff != "" {
	// 	ips := strings.Split(xff, ",")
	// 	remoteAddr = strings.TrimSpace(ips[0])
	// } else if xrip := r.Header.Get("X-Real-IP"); xrip != "" {
	// 	remoteAddr = xrip
	// }

	// fmt.Println("Endereço IP de origem:", remoteAddr)

	// for name, values := range r.Header {
	// 	for _, value := range values {
	// 		fmt.Printf("%s: %s\n", name, value)
	// 	}
	// }

	// somente valido para quando forem diferentes os tipos de retorno
	// var getFuncHttp connect.GetFuncHttp // byte, string , any
	// var getFuncGrpc connect.GetFuncGrpc // byte, string , any
	// if _, err := strconv.Atoi(param); err == nil {
	// 	if protocol == "http" {
	// 		getFuncHttp = connect.GetByIDHttp
	// 	} else {
	// 		getFuncGrpc = connect.GetByIDGrpc
	// 	}
	// } else {
	// 	if protocol == "http" {
	// 		getFuncHttp = connect.GetByNameHttp
	// 	} else {
	// 		getFuncGrpc = connect.GetByNameGrpc
	// 	}
	// }

	var getFunc connect.GetFunc // byte, string , any
	if _, err := strconv.Atoi(param); err == nil {
		getFunc = connect.GetByID
	} else {
		getFunc = connect.GetByName
	}

	data, status, err := getFunc(connect.Env(env), protocol, param) // byte, string , any
	if err != nil {
		return c.Status(status).JSON(e.Parse([]byte(err.Error())))
	}

	rawFilters := c.Query["filters"]
	if rawFilters != "" {
		filters := strings.Split(rawFilters, ",")
		data2, err := filterFields(data, filters) // byte, string , any
		if err != nil {
			return c.Status(status).JSON(e.NewErrorFrom(err))
		}
		return c.Status(http.StatusOK).JSON(data2)
	}

	return c.Status(http.StatusOK).Send(data) //byte

	// somente valido para quando forem diferentes os tipos de retorno
	// if protocol == "http" {
	// 	data, status, err := getFuncHttp(protocol, param) // byte, string , any
	// 	if err != nil {
	// 		return c.Status(status).JSON(e.Parse([]byte(err.Error())))
	// 	}
	// 	return c.Status(http.StatusOK).Send(data) //byte
	// } else {
	// 	data, status, err := getFuncGrpc(protocol, param) // byte, string , any
	// 	if err != nil {
	// 		return c.Status(status).JSON(e.Parse([]byte(err.Error())))
	// 	}
	// 	return c.Status(http.StatusOK).Send(data) //byte
	// }

	// return c.Status(http.StatusOK).SendString(data) // string
	// return c.Status(http.StatusOK).JSON(data) // struct
	// return c.Status(http.StatusOK).Send(data) //byte
}

// func filterFields(hero any, filters []string) (any, error) {
// func filterFields(hero string, filters []string) (any, error) {
func filterFields(hero []byte, filters []string) (interface{}, error) { // bug 1.6 Go
	var heroMap map[string]interface{}
	// heroMarshal, err := json.Marshal(hero)
	// if err != nil {
	// 	return nil, err
	// }

	// if err := json.Unmarshal([]byte(hero), &heroMap); err != nil { // string
	if err := json.Unmarshal(hero, &heroMap); err != nil { // byte
		return "", err
	}

	filterMap := make(map[string]any)
	for _, v := range filters {
		field, ok := heroMap[v]
		if !ok {
			continue
		}
		filterMap[v] = field
	}

	// b, err := json.Marshal(filterMap)
	// if err != nil {
	// 	return "", err
	// }
	// return b, nil
	// return string(b), nil // string
	return filterMap, nil
}
