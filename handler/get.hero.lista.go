package handler

import (
	"net/http"
	"strings"

	"github.com/jeffotoni/quick"
	e "gitlab.com/jeffotoni1/eventogo/sdk/error"
	"gitlab.com/jeffotoni1/eventogo/sdk/fastdata/zerohero/connect"
)

func (env Env) GetHeroList(c *quick.Ctx) error {
	c.Set("Engine", "Quick")
	c.Set("Content-Type", "application/json")

	protocol := strings.ToLower(c.Param("protocol"))
	if protocol == "" {
		return c.Status(http.StatusBadRequest).JSON(e.NewError("URL protocol not provided."))
	}

	if protocol != "http" && protocol != "grpc" {
		return c.Status(http.StatusBadRequest).JSON(e.NewError("protocol not provided [http/grpc]."))
	}

	var getFunc = connect.GetByNameList
	data, status, err := getFunc(connect.Env(env), protocol, "list") // byte, string , any
	if err != nil {
		return c.Status(status).JSON(e.Parse([]byte(err.Error())))
	}

	return c.Status(http.StatusOK).Send(data) //byte
}
