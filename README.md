# gapi

Gapi é uma api rEST funcionando em http1.1, http2.0 e possui a comunicação entre serviços via gRPC.

## Endpoints

```bash
[GET] /v1/:protocol/hero/:param
[GET] /ping
```

## Environment variables 

```bash
export GAPI_DOMAIN='0.0.0.0'
export GAPI_PORT='8080'
```

## Deploy

```bash
$ docker build -f Dockerfile --no-cache --build-arg NETRCUSER=$USER2 --build-arg NETRCPASS=$TOKEN2 -t jeffotoni/gapi .
```

```bash
$ docker run --name gapi --rm -p 8080:8080 jeffotoni/gapi
```

## Curls for testing

```bash
# Get hero by ID via REST

$ curl -i -XGET localhost:8080/v1/http/hero/13

HTTP/1.1 200 OK
Content-Type: application/json
Engine: Quick
Msguuid: 11edc59b-1f77-41e5-933f-4bcd0045d349
Date: Fri, 05 May 2023 05:19:56 GMT
Content-Length: 730
```

```bash
# Get hero by name via REST

$ curl -i -XGET localhost:8080/v1/http/hero/ajax

HTTP/1.1 200 OK
Content-Type: application/json
Engine: Quick
Msguuid: 11edc59b-1f77-41e5-933f-4bcd0045d349
Date: Fri, 05 May 2023 05:19:56 GMT
Content-Length: 730
```

```bash
# Get hero by ID via gRPC

$ curl -i -XGET localhost:8080/v1/grpc/hero/13

HTTP/1.1 200 OK
Content-Type: application/json
Engine: Quick
Msguuid: 11edc59b-1f77-41e5-933f-4bcd0045d349
Date: Fri, 05 May 2023 05:19:56 GMT
Content-Length: 730
```

```bash
# Get hero by name via gRPC

$ curl -i -XGET localhost:8080/v1/grpc/hero/ajax

HTTP/1.1 200 OK
Content-Type: application/json
Engine: Quick
Msguuid: 11edc59b-1f77-41e5-933f-4bcd0045d349
Date: Fri, 05 May 2023 05:19:56 GMT
Content-Length: 730
```

### Stress wrk

#### Byte Hero Grpc | Http - GetName | GetID

Este teste nossa comunicação entre serviços é retorando byte, o grpc retona byte em sua resposta.

```bash
gfastdata git:(main) wrk -t12 -c100 -d15s http://localhost:8080/v1/grpc/hero/ajax
Running 15s test @ http://localhost:8080/v1/grpc/hero/ajax
  12 threads and 100 connections
  Thread Stats   Avg      Stdev     Max   +/- Stdev
    Latency     1.27ms    0.87ms  41.19ms   95.13%
    Req/Sec     6.46k   615.32    12.42k    88.38%
  1161966 requests in 15.10s, 0.98GB read
Requests/sec:  76952.79
Transfer/sec:     66.12MB
➜  gfastdata git:(main) wrk -t12 -c100 -d15s http://localhost:8080/v1/grpc/hero/13  
Running 15s test @ http://localhost:8080/v1/grpc/hero/13
  12 threads and 100 connections
  Thread Stats   Avg      Stdev     Max   +/- Stdev
    Latency     1.21ms  448.87us  11.86ms   80.71%
    Req/Sec     6.66k   589.66    10.56k    94.68%
  1197063 requests in 15.06s, 1.00GB read
Requests/sec:  79491.58
Transfer/sec:     68.30MB
➜  gfastdata git:(main) wrk -t12 -c100 -d15s http://localhost:8080/v1/http/hero/13
Running 15s test @ http://localhost:8080/v1/http/hero/13
  12 threads and 100 connections
  Thread Stats   Avg      Stdev     Max   +/- Stdev
    Latency     5.13ms    7.57ms 111.87ms   89.37%
    Req/Sec     2.65k     1.48k    7.98k    57.64%
  476946 requests in 15.08s, 409.82MB read
Requests/sec:  31621.37
Transfer/sec:     27.17MB
➜  gfastdata git:(main) wrk -t12 -c100 -d15s http://localhost:8080/v1/http/hero/ajax
Running 15s test @ http://localhost:8080/v1/http/hero/ajax
  12 threads and 100 connections
  Thread Stats   Avg      Stdev     Max   +/- Stdev
    Latency     5.12ms    7.79ms 109.66ms   89.85%
    Req/Sec     2.67k     1.49k   15.14k    62.47%
  479535 requests in 15.09s, 412.05MB read
Requests/sec:  31770.69
Transfer/sec:     27.30MB

```


### Stress K6

#### Struct Hero Http - GetName

```bash
 k6 run -d 90s -u 100 k6/http.get.hero.by.name.js

          /\      |‾‾| /‾‾/   /‾‾/   
     /\  /  \     |  |/  /   /  /    
    /  \/    \    |     (   /   ‾‾\  
   /          \   |  |\  \ |  (‾)  | 
  / __________ \  |__| \__\ \_____/ .io

  execution: local
     script: k6/http.get.hero.by.name.js
     output: -

  scenarios: (100.00%) 1 scenario, 100 max VUs, 2m0s max duration (incl. graceful stop):
           * default: 100 looping VUs for 1m30s (gracefulStop: 30s)


running (1m30.0s), 000/100 VUs, 2147933 complete and 0 interrupted iterations
default ✓ [======================================] 100 VUs  1m30s

     data_received..................: 1.9 GB  22 MB/s
     data_sent......................: 277 MB  3.1 MB/s
     http_req_blocked...............: avg=2.32µs  min=655ns    med=1.52µs  max=43.56ms  p(90)=1.98µs  p(95)=2.26µs 
     http_req_connecting............: avg=224ns   min=0s       med=0s      max=42.61ms  p(90)=0s      p(95)=0s     
     http_req_duration..............: avg=4.12ms  min=140.43µs med=3.1ms   max=158.14ms p(90)=6.75ms  p(95)=9.51ms 
       { expected_response:true }...: avg=4.12ms  min=140.43µs med=3.1ms   max=158.14ms p(90)=6.75ms  p(95)=9.51ms 
     http_req_failed................: 0.00%   ✓ 0            ✗ 2147933
     http_req_receiving.............: avg=27.98µs min=7.63µs   med=18.17µs max=21.67ms  p(90)=25.56µs p(95)=29.9µs 
     http_req_sending...............: avg=10.75µs min=3.38µs   med=7.56µs  max=42.77ms  p(90)=9.33µs  p(95)=10.52µs
     http_req_tls_handshaking.......: avg=0s      min=0s       med=0s      max=0s       p(90)=0s      p(95)=0s     
     http_req_waiting...............: avg=4.08ms  min=124.34µs med=3.07ms  max=158.11ms p(90)=6.69ms  p(95)=9.44ms 
     http_reqs......................: 2147933 23865.257501/s
     iteration_duration.............: avg=4.18ms  min=168.85µs med=3.16ms  max=158.2ms  p(90)=6.82ms  p(95)=9.59ms 
     iterations.....................: 2147933 23865.257501/s
     vus............................: 100     min=100        max=100  
     vus_max........................: 100     min=100        max=100  

```

#### Struct Hero Http - GetID

```bash
k6 run -d 90s -u 100 k6/http.get.hero.by.id.js 

          /\      |‾‾| /‾‾/   /‾‾/   
     /\  /  \     |  |/  /   /  /    
    /  \/    \    |     (   /   ‾‾\  
   /          \   |  |\  \ |  (‾)  | 
  / __________ \  |__| \__\ \_____/ .io

  execution: local
     script: k6/http.get.hero.by.id.js
     output: -

  scenarios: (100.00%) 1 scenario, 100 max VUs, 2m0s max duration (incl. graceful stop):
           * default: 100 looping VUs for 1m30s (gracefulStop: 30s)


running (1m30.0s), 000/100 VUs, 2144279 complete and 0 interrupted iterations
default ✓ [======================================] 100 VUs  1m30s

     data_received..................: 1.9 GB  22 MB/s
     data_sent......................: 272 MB  3.0 MB/s
     http_req_blocked...............: avg=2.36µs  min=709ns    med=1.52µs  max=36.13ms  p(90)=1.99µs  p(95)=2.31µs 
     http_req_connecting............: avg=220ns   min=0s       med=0s      max=36.02ms  p(90)=0s      p(95)=0s     
     http_req_duration..............: avg=4.12ms  min=143.19µs med=3.11ms  max=157.93ms p(90)=6.79ms  p(95)=9.49ms 
       { expected_response:true }...: avg=4.12ms  min=143.19µs med=3.11ms  max=157.93ms p(90)=6.79ms  p(95)=9.49ms 
     http_req_failed................: 0.00%   ✓ 0            ✗ 2144279
     http_req_receiving.............: avg=28.55µs min=8.74µs   med=18.25µs max=24.15ms  p(90)=25.94µs p(95)=30.95µs
     http_req_sending...............: avg=10.97µs min=3.72µs   med=7.56µs  max=36.36ms  p(90)=9.38µs  p(95)=10.9µs 
     http_req_tls_handshaking.......: avg=0s      min=0s       med=0s      max=0s       p(90)=0s      p(95)=0s     
     http_req_waiting...............: avg=4.08ms  min=124.21µs med=3.08ms  max=157.91ms p(90)=6.74ms  p(95)=9.42ms 
     http_reqs......................: 2144279 23824.280905/s
     iteration_duration.............: avg=4.18ms  min=177.21µs med=3.17ms  max=157.97ms p(90)=6.86ms  p(95)=9.57ms 
     iterations.....................: 2144279 23824.280905/s
     vus............................: 100     min=100        max=100  
     vus_max........................: 100     min=100        max=100  


```

#### string http - GetName
```bash
k6 run -d 90s -u 100 k6/http.get.hero.by.name.js

          /\      |‾‾| /‾‾/   /‾‾/   
     /\  /  \     |  |/  /   /  /    
    /  \/    \    |     (   /   ‾‾\  
   /          \   |  |\  \ |  (‾)  | 
  / __________ \  |__| \__\ \_____/ .io

  execution: local
     script: k6/http.get.hero.by.name.js
     output: -

  scenarios: (100.00%) 1 scenario, 100 max VUs, 2m0s max duration (incl. graceful stop):
           * default: 100 looping VUs for 1m30s (gracefulStop: 30s)


running (1m30.0s), 000/100 VUs, 2165230 complete and 0 interrupted iterations
default ✓ [======================================] 100 VUs  1m30s

     data_received..................: 2.0 GB  22 MB/s
     data_sent......................: 279 MB  3.1 MB/s
     http_req_blocked...............: avg=3.17µs  min=673ns    med=1.52µs  max=33.84ms  p(90)=1.98µs  p(95)=2.27µs 
     http_req_connecting............: avg=1.03µs  min=0s       med=0s      max=33.3ms   p(90)=0s      p(95)=0s     
     http_req_duration..............: avg=4.08ms  min=99.14µs  med=3.05ms  max=145.18ms p(90)=6.79ms  p(95)=9.54ms 
       { expected_response:true }...: avg=4.08ms  min=99.14µs  med=3.05ms  max=145.18ms p(90)=6.79ms  p(95)=9.54ms 
     http_req_failed................: 0.00%   ✓ 0            ✗ 2165230
     http_req_receiving.............: avg=28.25µs min=7.24µs   med=18.12µs max=18.5ms   p(90)=25.67µs p(95)=30.55µs
     http_req_sending...............: avg=10.84µs min=3.46µs   med=7.5µs   max=15.09ms  p(90)=9.32µs  p(95)=10.72µs
     http_req_tls_handshaking.......: avg=0s      min=0s       med=0s      max=0s       p(90)=0s      p(95)=0s     
     http_req_waiting...............: avg=4.04ms  min=83.51µs  med=3.01ms  max=145.16ms p(90)=6.73ms  p(95)=9.48ms 
     http_reqs......................: 2165230 24057.627034/s
     iteration_duration.............: avg=4.14ms  min=125.28µs med=3.1ms   max=145.22ms p(90)=6.86ms  p(95)=9.62ms 
     iterations.....................: 2165230 24057.627034/s
     vus............................: 100     min=100        max=100  
     vus_max........................: 100     min=100        max=100  



```

#### string http - GetID
```bash
k6 run -d 90s -u 100 k6/http.get.hero.by.id.js 

          /\      |‾‾| /‾‾/   /‾‾/   
     /\  /  \     |  |/  /   /  /    
    /  \/    \    |     (   /   ‾‾\  
   /          \   |  |\  \ |  (‾)  | 
  / __________ \  |__| \__\ \_____/ .io

  execution: local
     script: k6/http.get.hero.by.id.js
     output: -

  scenarios: (100.00%) 1 scenario, 100 max VUs, 2m0s max duration (incl. graceful stop):
           * default: 100 looping VUs for 1m30s (gracefulStop: 30s)


running (1m30.0s), 000/100 VUs, 2159413 complete and 0 interrupted iterations
default ✓ [======================================] 100 VUs  1m30s

     data_received..................: 1.9 GB  22 MB/s
     data_sent......................: 274 MB  3.0 MB/s
     http_req_blocked...............: avg=2.37µs  min=713ns    med=1.52µs  max=36.45ms  p(90)=1.98µs  p(95)=2.27µs 
     http_req_connecting............: avg=152ns   min=0s       med=0s      max=29.46ms  p(90)=0s      p(95)=0s     
     http_req_duration..............: avg=4.09ms  min=135.67µs med=3.04ms  max=155.29ms p(90)=6.79ms  p(95)=9.6ms  
       { expected_response:true }...: avg=4.09ms  min=135.67µs med=3.04ms  max=155.29ms p(90)=6.79ms  p(95)=9.6ms  
     http_req_failed................: 0.00%   ✓ 0            ✗ 2159413
     http_req_receiving.............: avg=28.61µs min=7.53µs   med=18.14µs max=25.75ms  p(90)=25.72µs p(95)=30.76µs
     http_req_sending...............: avg=10.94µs min=3.72µs   med=7.5µs   max=23.29ms  p(90)=9.28µs  p(95)=10.63µs
     http_req_tls_handshaking.......: avg=0s      min=0s       med=0s      max=0s       p(90)=0s      p(95)=0s     
     http_req_waiting...............: avg=4.05ms  min=118.29µs med=3ms     max=155.27ms p(90)=6.74ms  p(95)=9.54ms 
     http_reqs......................: 2159413 23992.070785/s
     iteration_duration.............: avg=4.15ms  min=177.19µs med=3.1ms   max=155.33ms p(90)=6.86ms  p(95)=9.69ms 
     iterations.....................: 2159413 23992.070785/s
     vus............................: 100     min=100        max=100  
     vus_max........................: 100     min=100        max=100  

```


#### Struct Hero grpc - GetName

```bash
k6 run -d 90s -u 100 k6/grpc.get.hero.by.name.js 

          /\      |‾‾| /‾‾/   /‾‾/   
     /\  /  \     |  |/  /   /  /    
    /  \/    \    |     (   /   ‾‾\  
   /          \   |  |\  \ |  (‾)  | 
  / __________ \  |__| \__\ \_____/ .io

  execution: local
     script: k6/grpc.get.hero.by.name.js
     output: -

  scenarios: (100.00%) 1 scenario, 100 max VUs, 2m0s max duration (incl. graceful stop):
           * default: 100 looping VUs for 1m30s (gracefulStop: 30s)


running (1m30.0s), 000/100 VUs, 3953207 complete and 0 interrupted iterations
default ✓ [======================================] 100 VUs  1m30s

     data_received..................: 3.6 GB  40 MB/s
     data_sent......................: 510 MB  5.7 MB/s
     http_req_blocked...............: avg=1.98µs  min=632ns    med=1.48µs  max=36.59ms p(90)=1.88µs  p(95)=2.15µs 
     http_req_connecting............: avg=82ns    min=0s       med=0s      max=29.54ms p(90)=0s      p(95)=0s     
     http_req_duration..............: avg=2.21ms  min=156.86µs med=2.09ms  max=84.03ms p(90)=3.26ms  p(95)=3.73ms 
       { expected_response:true }...: avg=2.21ms  min=156.86µs med=2.09ms  max=84.03ms p(90)=3.26ms  p(95)=3.73ms 
     http_req_failed................: 0.00%   ✓ 0            ✗ 3953207
     http_req_receiving.............: avg=24.53µs min=7.23µs   med=17.23µs max=81.42ms p(90)=23.29µs p(95)=27.19µs
     http_req_sending...............: avg=9.71µs  min=3.34µs   med=7.33µs  max=29.77ms p(90)=8.88µs  p(95)=9.82µs 
     http_req_tls_handshaking.......: avg=0s      min=0s       med=0s      max=0s      p(90)=0s      p(95)=0s     
     http_req_waiting...............: avg=2.18ms  min=139.56µs med=2.06ms  max=73.86ms p(90)=3.22ms  p(95)=3.68ms 
     http_reqs......................: 3953207 43923.654111/s
     iteration_duration.............: avg=2.26ms  min=176.52µs med=2.14ms  max=84.08ms p(90)=3.32ms  p(95)=3.79ms 
     iterations.....................: 3953207 43923.654111/s
     vus............................: 100     min=100        max=100  
     vus_max........................: 100     min=100        max=100  
```

#### Struct Hero grpc - GetID

```bash
 k6 run -d 90s -u 100 k6/grpc.get.hero.by.id.js  

          /\      |‾‾| /‾‾/   /‾‾/   
     /\  /  \     |  |/  /   /  /    
    /  \/    \    |     (   /   ‾‾\  
   /          \   |  |\  \ |  (‾)  | 
  / __________ \  |__| \__\ \_____/ .io

  execution: local
     script: k6/grpc.get.hero.by.id.js
     output: -

  scenarios: (100.00%) 1 scenario, 100 max VUs, 2m0s max duration (incl. graceful stop):
           * default: 100 looping VUs for 1m30s (gracefulStop: 30s)


running (1m30.0s), 000/100 VUs, 3940472 complete and 0 interrupted iterations
default ✓ [======================================] 100 VUs  1m30s

     data_received..................: 3.6 GB  39 MB/s
     data_sent......................: 500 MB  5.6 MB/s
     http_req_blocked...............: avg=2.04µs  min=646ns    med=1.48µs  max=35.41ms p(90)=1.89µs  p(95)=2.17µs 
     http_req_connecting............: avg=113ns   min=0s       med=0s      max=31.84ms p(90)=0s      p(95)=0s     
     http_req_duration..............: avg=2.22ms  min=150.84µs med=2.1ms   max=25.22ms p(90)=3.28ms  p(95)=3.76ms 
       { expected_response:true }...: avg=2.22ms  min=150.84µs med=2.1ms   max=25.22ms p(90)=3.28ms  p(95)=3.76ms 
     http_req_failed................: 0.00%   ✓ 0           ✗ 3940472
     http_req_receiving.............: avg=24.43µs min=7.16µs   med=17.29µs max=10.54ms p(90)=23.46µs p(95)=27.61µs
     http_req_sending...............: avg=9.71µs  min=3.47µs   med=7.31µs  max=10.15ms p(90)=8.85µs  p(95)=9.85µs 
     http_req_tls_handshaking.......: avg=0s      min=0s       med=0s      max=0s      p(90)=0s      p(95)=0s     
     http_req_waiting...............: avg=2.18ms  min=137.18µs med=2.07ms  max=25.14ms p(90)=3.25ms  p(95)=3.72ms 
     http_reqs......................: 3940472 43781.88601/s
     iteration_duration.............: avg=2.27ms  min=172.22µs med=2.15ms  max=37.71ms p(90)=3.34ms  p(95)=3.83ms 
     iterations.....................: 3940472 43781.88601/s
     vus............................: 100     min=100       max=100  
     vus_max........................: 100     min=100       max=100  

```


#### string grpc - GetName

```bash
 k6 run -d 90s -u 100 k6/grpc.get.hero.by.name.js

          /\      |‾‾| /‾‾/   /‾‾/   
     /\  /  \     |  |/  /   /  /    
    /  \/    \    |     (   /   ‾‾\  
   /          \   |  |\  \ |  (‾)  | 
  / __________ \  |__| \__\ \_____/ .io

  execution: local
     script: k6/grpc.get.hero.by.name.js
     output: -

  scenarios: (100.00%) 1 scenario, 100 max VUs, 2m0s max duration (incl. graceful stop):
           * default: 100 looping VUs for 1m30s (gracefulStop: 30s)


running (1m30.0s), 000/100 VUs, 4241246 complete and 0 interrupted iterations
default ✓ [======================================] 100 VUs  1m30s

     data_received..................: 3.8 GB  43 MB/s
     data_sent......................: 547 MB  6.1 MB/s
     http_req_blocked...............: avg=2.74µs  min=662ns    med=1.47µs  max=125.49ms p(90)=1.86µs  p(95)=2.12µs 
     http_req_connecting............: avg=699ns   min=0s       med=0s      max=45.99ms  p(90)=0s      p(95)=0s     
     http_req_duration..............: avg=2.05ms  min=166.01µs med=1.93ms  max=132.74ms p(90)=3.03ms  p(95)=3.45ms 
       { expected_response:true }...: avg=2.05ms  min=166.01µs med=1.93ms  max=132.74ms p(90)=3.03ms  p(95)=3.45ms 
     http_req_failed................: 0.00%   ✓ 0            ✗ 4241246
     http_req_receiving.............: avg=24.87µs min=7.59µs   med=17.08µs max=125.79ms p(90)=22.85µs p(95)=26.59µs
     http_req_sending...............: avg=9.64µs  min=3.46µs   med=7.27µs  max=85.67ms  p(90)=8.75µs  p(95)=9.59µs 
     http_req_tls_handshaking.......: avg=0s      min=0s       med=0s      max=0s       p(90)=0s      p(95)=0s     
     http_req_waiting...............: avg=2.02ms  min=139.09µs med=1.9ms   max=90.69ms  p(90)=2.99ms  p(95)=3.41ms 
     http_reqs......................: 4241246 47124.206843/s
     iteration_duration.............: avg=2.11ms  min=208.32µs med=1.98ms  max=136.26ms p(90)=3.09ms  p(95)=3.51ms 
     iterations.....................: 4241246 47124.206843/s
     vus............................: 100     min=100        max=100  
     vus_max........................: 100     min=100        max=100  

```
#### byte http - GetName

```bash
k6 run -d 90s -u 100 k6/http.get.hero.by.name.js

          /\      |‾‾| /‾‾/   /‾‾/   
     /\  /  \     |  |/  /   /  /    
    /  \/    \    |     (   /   ‾‾\  
   /          \   |  |\  \ |  (‾)  | 
  / __________ \  |__| \__\ \_____/ .io

  execution: local
     script: k6/http.get.hero.by.name.js
     output: -

  scenarios: (100.00%) 1 scenario, 100 max VUs, 2m0s max duration (incl. graceful stop):
           * default: 100 looping VUs for 1m30s (gracefulStop: 30s)


running (1m30.0s), 000/100 VUs, 2164505 complete and 0 interrupted iterations
default ✓ [======================================] 100 VUs  1m30s

     data_received..................: 2.0 GB  22 MB/s
     data_sent......................: 279 MB  3.1 MB/s
     http_req_blocked...............: avg=2.57µs  min=724ns    med=1.51µs  max=40.06ms  p(90)=1.96µs  p(95)=2.24µs 
     http_req_connecting............: avg=166ns   min=0s       med=0s      max=20.91ms  p(90)=0s      p(95)=0s     
     http_req_duration..............: avg=4.08ms  min=147.67µs med=3.02ms  max=134.39ms p(90)=6.7ms   p(95)=9.63ms 
       { expected_response:true }...: avg=4.08ms  min=147.67µs med=3.02ms  max=134.39ms p(90)=6.7ms   p(95)=9.63ms 
     http_req_failed................: 0.00%   ✓ 0            ✗ 2164505
     http_req_receiving.............: avg=28.31µs min=8.76µs   med=18.04µs max=33.35ms  p(90)=25.45µs p(95)=30.03µs
     http_req_sending...............: avg=10.91µs min=3.85µs   med=7.48µs  max=14.58ms  p(90)=9.26µs  p(95)=10.5µs 
     http_req_tls_handshaking.......: avg=0s      min=0s       med=0s      max=0s       p(90)=0s      p(95)=0s     
     http_req_waiting...............: avg=4.05ms  min=120.41µs med=2.98ms  max=134.36ms p(90)=6.64ms  p(95)=9.57ms 
     http_reqs......................: 2164505 24049.612254/s
     iteration_duration.............: avg=4.14ms  min=183.01µs med=3.07ms  max=134.43ms p(90)=6.77ms  p(95)=9.72ms 
     iterations.....................: 2164505 24049.612254/s
     vus............................: 100     min=100        max=100  
     vus_max........................: 100     min=100        max=100  

```

#### byte http - GetID

```bash
k6 run -d 90s -u 100 k6/http.get.hero.by.id.js 

          /\      |‾‾| /‾‾/   /‾‾/   
     /\  /  \     |  |/  /   /  /    
    /  \/    \    |     (   /   ‾‾\  
   /          \   |  |\  \ |  (‾)  | 
  / __________ \  |__| \__\ \_____/ .io

  execution: local
     script: k6/http.get.hero.by.id.js
     output: -

  scenarios: (100.00%) 1 scenario, 100 max VUs, 2m0s max duration (incl. graceful stop):
           * default: 100 looping VUs for 1m30s (gracefulStop: 30s)


running (1m30.0s), 000/100 VUs, 2149655 complete and 0 interrupted iterations
default ✓ [======================================] 100 VUs  1m30s

     data_received..................: 1.9 GB  22 MB/s
     data_sent......................: 273 MB  3.0 MB/s
     http_req_blocked...............: avg=2.31µs  min=657ns    med=1.53µs max=28.46ms  p(90)=2µs     p(95)=2.32µs 
     http_req_connecting............: avg=137ns   min=0s       med=0s     max=28.43ms  p(90)=0s      p(95)=0s     
     http_req_duration..............: avg=4.11ms  min=120.17µs med=3.07ms max=187.38ms p(90)=6.77ms  p(95)=9.5ms  
       { expected_response:true }...: avg=4.11ms  min=120.17µs med=3.07ms max=187.38ms p(90)=6.77ms  p(95)=9.5ms  
     http_req_failed................: 0.00%   ✓ 0           ✗ 2149655
     http_req_receiving.............: avg=29.8µs  min=6.54µs   med=18.3µs max=108.67ms p(90)=26.15µs p(95)=31.37µs
     http_req_sending...............: avg=11.27µs min=3.48µs   med=7.58µs max=30.34ms  p(90)=9.45µs  p(95)=11.11µs
     http_req_tls_handshaking.......: avg=0s      min=0s       med=0s     max=0s       p(90)=0s      p(95)=0s     
     http_req_waiting...............: avg=4.07ms  min=106.99µs med=3.03ms max=187.3ms  p(90)=6.71ms  p(95)=9.43ms 
     http_reqs......................: 2149655 23884.02339/s
     iteration_duration.............: avg=4.17ms  min=145.08µs med=3.12ms max=187.45ms p(90)=6.84ms  p(95)=9.59ms 
     iterations.....................: 2149655 23884.02339/s
     vus............................: 100     min=100       max=100  
     vus_max........................: 100     min=100       max=100  

```

#### byte grpc - GetName

```bash
 k6 run -d 90s -u 100 k6/grpc.get.hero.by.name.js 

          /\      |‾‾| /‾‾/   /‾‾/   
     /\  /  \     |  |/  /   /  /    
    /  \/    \    |     (   /   ‾‾\  
   /          \   |  |\  \ |  (‾)  | 
  / __________ \  |__| \__\ \_____/ .io

  execution: local
     script: k6/grpc.get.hero.by.name.js
     output: -

  scenarios: (100.00%) 1 scenario, 100 max VUs, 2m0s max duration (incl. graceful stop):
           * default: 100 looping VUs for 1m30s (gracefulStop: 30s)


running (1m30.0s), 000/100 VUs, 4339334 complete and 0 interrupted iterations
default ✓ [======================================] 100 VUs  1m30s

     data_received..................: 3.9 GB  43 MB/s
     data_sent......................: 560 MB  6.2 MB/s
     http_req_blocked...............: avg=2.14µs  min=652ns    med=1.45µs  max=99.45ms  p(90)=1.84µs p(95)=2.1µs  
     http_req_connecting............: avg=89ns    min=0s       med=0s      max=33.7ms   p(90)=0s     p(95)=0s     
     http_req_duration..............: avg=2.01ms  min=164.18µs med=1.89ms  max=144.56ms p(90)=2.96ms p(95)=3.36ms 
       { expected_response:true }...: avg=2.01ms  min=164.18µs med=1.89ms  max=144.56ms p(90)=2.96ms p(95)=3.36ms 
     http_req_failed................: 0.00%   ✓ 0            ✗ 4339334
     http_req_receiving.............: avg=24.78µs min=7.12µs   med=16.87µs max=139.73ms p(90)=22.6µs p(95)=26.28µs
     http_req_sending...............: avg=9.66µs  min=3.5µs    med=7.19µs  max=98.81ms  p(90)=8.68µs p(95)=9.55µs 
     http_req_tls_handshaking.......: avg=0s      min=0s       med=0s      max=0s       p(90)=0s     p(95)=0s     
     http_req_waiting...............: avg=1.97ms  min=138.92µs med=1.86ms  max=144.09ms p(90)=2.92ms p(95)=3.32ms 
     http_reqs......................: 4339334 48213.684411/s
     iteration_duration.............: avg=2.06ms  min=232.64µs med=1.94ms  max=144.61ms p(90)=3.01ms p(95)=3.43ms 
     iterations.....................: 4339334 48213.684411/s
     vus............................: 100     min=100        max=100  
     vus_max........................: 100     min=100        max=100  

```


#### string grpc - GetID

```bash
 k6 run -d 90s -u 100 k6/grpc.get.hero.by.name.js 

          /\      |‾‾| /‾‾/   /‾‾/   
     /\  /  \     |  |/  /   /  /    
    /  \/    \    |     (   /   ‾‾\  
   /          \   |  |\  \ |  (‾)  | 
  / __________ \  |__| \__\ \_____/ .io

  execution: local
     script: k6/grpc.get.hero.by.name.js
     output: -

  scenarios: (100.00%) 1 scenario, 100 max VUs, 2m0s max duration (incl. graceful stop):
           * default: 100 looping VUs for 1m30s (gracefulStop: 30s)


running (1m30.0s), 000/100 VUs, 4233460 complete and 0 interrupted iterations
default ✓ [======================================] 100 VUs  1m30s

     data_received..................: 3.8 GB  42 MB/s
     data_sent......................: 546 MB  6.1 MB/s
     http_req_blocked...............: avg=1.99µs  min=648ns    med=1.47µs  max=127.98ms p(90)=1.86µs p(95)=2.13µs 
     http_req_connecting............: avg=80ns    min=0s       med=0s      max=30.57ms  p(90)=0s     p(95)=0s     
     http_req_duration..............: avg=2.06ms  min=128.29µs med=1.94ms  max=140.75ms p(90)=3.05ms p(95)=3.48ms 
       { expected_response:true }...: avg=2.06ms  min=128.29µs med=1.94ms  max=140.75ms p(90)=3.05ms p(95)=3.48ms 
     http_req_failed................: 0.00%   ✓ 0          ✗ 4233460
     http_req_receiving.............: avg=25.53µs min=7.33µs   med=17.07µs max=138.95ms p(90)=23µs   p(95)=27.01µs
     http_req_sending...............: avg=9.76µs  min=3.36µs   med=7.27µs  max=135.01ms p(90)=8.78µs p(95)=9.72µs 
     http_req_tls_handshaking.......: avg=0s      min=0s       med=0s      max=0s       p(90)=0s     p(95)=0s     
     http_req_waiting...............: avg=2.02ms  min=112.81µs med=1.91ms  max=132.76ms p(90)=3.01ms p(95)=3.43ms 
     http_reqs......................: 4233460 47037.5015/s
     iteration_duration.............: avg=2.11ms  min=154.98µs med=1.99ms  max=141.29ms p(90)=3.11ms p(95)=3.54ms 
     iterations.....................: 4233460 47037.5015/s
     vus............................: 100     min=100      max=100  
     vus_max........................: 100     min=100      max=100  

```

#### byte grpc - GetID

```bash
 k6 run -d 90s -u 100 k6/grpc.get.hero.by.id.js

          /\      |‾‾| /‾‾/   /‾‾/   
     /\  /  \     |  |/  /   /  /    
    /  \/    \    |     (   /   ‾‾\  
   /          \   |  |\  \ |  (‾)  | 
  / __________ \  |__| \__\ \_____/ .io

  execution: local
     script: k6/grpc.get.hero.by.id.js
     output: -

  scenarios: (100.00%) 1 scenario, 100 max VUs, 2m0s max duration (incl. graceful stop):
           * default: 100 looping VUs for 1m30s (gracefulStop: 30s)


running (1m30.0s), 000/100 VUs, 4423142 complete and 0 interrupted iterations
default ✓ [======================================] 100 VUs  1m30s

     data_received..................: 4.0 GB  44 MB/s
     data_sent......................: 562 MB  6.2 MB/s
     http_req_blocked...............: avg=1.96µs  min=618ns    med=1.45µs  max=86.33ms  p(90)=1.82µs  p(95)=2.05µs 
     http_req_connecting............: avg=102ns   min=0s       med=0s      max=35.21ms  p(90)=0s      p(95)=0s     
     http_req_duration..............: avg=1.97ms  min=170.12µs med=1.85ms  max=161.88ms p(90)=2.89ms  p(95)=3.29ms 
       { expected_response:true }...: avg=1.97ms  min=170.12µs med=1.85ms  max=161.88ms p(90)=2.89ms  p(95)=3.29ms 
     http_req_failed................: 0.00%   ✓ 0            ✗ 4423142
     http_req_receiving.............: avg=23.58µs min=6.89µs   med=16.58µs max=156.03ms p(90)=22.05µs p(95)=25.43µs
     http_req_sending...............: avg=9.5µs   min=3.29µs   med=7.11µs  max=155.96ms p(90)=8.52µs  p(95)=9.28µs 
     http_req_tls_handshaking.......: avg=0s      min=0s       med=0s      max=0s       p(90)=0s      p(95)=0s     
     http_req_waiting...............: avg=1.94ms  min=157.25µs med=1.82ms  max=161.54ms p(90)=2.86ms  p(95)=3.25ms 
     http_reqs......................: 4423142 49145.048955/s
     iteration_duration.............: avg=2.02ms  min=189.16µs med=1.9ms   max=162.45ms p(90)=2.95ms  p(95)=3.35ms 
     iterations.....................: 4423142 49145.048955/s
     vus............................: 100     min=100        max=100  
     vus_max........................: 100     min=100        max=100  


```