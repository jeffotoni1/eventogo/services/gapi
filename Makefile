# Makefile
#
#
.EXPORT_ALL_VARIABLES:
GO111MODULE=on
GOPROXY=direct
GOSUMDB=off
GOPRIVATE=gitlab.com/jeffotoni1/eventogo/services/gapi
update:
	@rm -f go*
	go mod init gitlab.com/jeffotoni1/eventogo/services/gapi
	go mod tidy
	@echo "\033[0;33m################ Go mod tidy #####################\033[0m"

gosec:
	gosec ./...

ineffassign:
	ineffassign ./...

staticcheck:
	staticcheck ./...

