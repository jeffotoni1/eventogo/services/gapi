FROM --platform=$BUILDPLATFORM golang:1.20.4-alpine AS builder

RUN apk add --no-cache git tzdata ca-certificates

ENV GO111MODULE=on \
  GOPROXY=direct \
  GOSUMDB=off \
  CGO_ENABLED=0 \
  GOOS=linux  \
  GOARCH=$GOARCH \
  GOPRIVATE=gitlab.com/jeffotoni1

WORKDIR /go/src

COPY . .

RUN CGO_ENABLED=0 go build \
  -trimpath \
  -buildvcs=false \
  -ldflags="-s -w" \
  -o /go/bin/main \
  .

#-----------------------------------------------------------------------------
FROM scratch

ENV GODEBUG=madvdontneed=1
ENV TZ=America/Sao_Paulo

COPY --from=builder /etc/ssl/certs/ca-certificates.crt /etc/ssl/certs/
COPY --from=builder /usr/share/zoneinfo /usr/share/zoneinfo
COPY --from=builder /go/bin/main .

ENTRYPOINT ["./main"]
