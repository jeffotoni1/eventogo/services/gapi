module gitlab.com/jeffotoni1/eventogo/services/gapi

go 1.20

require (
	github.com/jeffotoni/quick v0.0.0-20230511121806-a689c78a337c
	gitlab.com/jeffotoni1/eventogo/sdk/env v0.0.0-20230505213544-5522a4ae0847
	gitlab.com/jeffotoni1/eventogo/sdk/error v0.0.0-20230505213613-5640e6027738
	gitlab.com/jeffotoni1/eventogo/sdk/fastdata/zerohero v0.0.0-20230512223920-a50a9f32698d
	gitlab.com/jeffotoni1/eventogo/sdk/fmts v0.0.0-20230505213702-d48105a993cc
)

require (
	github.com/golang/protobuf v1.5.3 // indirect
	github.com/google/uuid v1.3.0 // indirect
	github.com/jeffotoni/gconcat v0.0.14 // indirect
	github.com/patrickmn/go-cache v2.1.0+incompatible // indirect
	golang.org/x/net v0.8.0 // indirect
	golang.org/x/sys v0.6.0 // indirect
	golang.org/x/text v0.8.0 // indirect
	google.golang.org/genproto v0.0.0-20230306155012-7f2fa6fef1f4 // indirect
	google.golang.org/grpc v1.55.0 // indirect
	google.golang.org/protobuf v1.30.0 // indirect
)
