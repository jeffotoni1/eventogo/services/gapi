package main

import (
	"log"

	"github.com/jeffotoni/quick"
	"github.com/jeffotoni/quick/middleware/cors"
	"github.com/jeffotoni/quick/middleware/logger"
	"github.com/jeffotoni/quick/middleware/msguuid"
	"gitlab.com/jeffotoni1/eventogo/sdk/env"
	"gitlab.com/jeffotoni1/eventogo/sdk/fastdata/zerohero/connect"
	"gitlab.com/jeffotoni1/eventogo/sdk/fmts"
	"gitlab.com/jeffotoni1/eventogo/services/gapi/handler"
)

var (
	GAPI_DOMAIN = env.GetString("GAPI_DOMAIN", "0.0.0.0")
	GAPI_PORT   = env.GetString("GAPI_PORT", "8080")
	GAPI_ADDR   = fmts.ConcatStr(GAPI_DOMAIN, ":", GAPI_PORT)

	GFASTDATA_GRPC_DOMAIN    = env.GetString("GFASTDATA_GRPC_DOMAIN", "0.0.0.0")
	GFASTDATA_GRPC_PORT      = env.GetString("GFASTDATA_GRPC_PORT", "50051")
	GFASTDATA_GRPC_NETWORK   = env.GetString("GFASTDATA_GRPC_NETWORK", "tcp")
	GFASTDATA_GRPC_LOG_LEVEL = env.GetString("GFASTDATA_GRPC_LOG_LEVEL", "")
	GFASTDATA_GRPC_ADDR      = fmts.ConcatStr(GFASTDATA_GRPC_DOMAIN, ":", GFASTDATA_GRPC_PORT)

	GFASTDATA_REST_DOMAIN          = env.GetString("GFASTDATA_REST_DOMAIN", "0.0.0.0")
	GFASTDATA_REST_PORT            = env.GetString("GFASTDATA_REST_PORT", "3000")
	GFASTDATA_REST_EXTERNAL_DOMAIN = env.GetString("GFASTDATA_REST_EXTERNAL_DOMAIN", "http://127.0.0.1")
	GFASTDATA_REST_PATH_NAME       = env.GetString("GFASTDATA_REST_PATH_NAME", "/v1/hero/name/")
	GFASTDATA_REST_PATH_NAME_LIST  = env.GetString("GFASTDATA_REST_PATH_NAME_LIST", "/v1/hero/list")
	GFASTDATA_REST_PATH_ID         = env.GetString("GFASTDATA_REST_PATH_ID", "/v1/hero/id/")
	GFASTDATA_REST_ADDR            = fmts.ConcatStr(GFASTDATA_REST_DOMAIN, ":", GFASTDATA_REST_PORT)
	GFASTDATA_REST_EXTERNAL_ADDR   = fmts.ConcatStr(GFASTDATA_REST_EXTERNAL_DOMAIN, ":", GFASTDATA_REST_PORT)

	GFASTDATA_REST_EXTERNAL_ADDR_LIST = fmts.ConcatStr(GFASTDATA_REST_EXTERNAL_DOMAIN, ":", GFASTDATA_REST_PORT)

	REST_URL_NAME = fmts.ConcatStr(
		GFASTDATA_REST_EXTERNAL_ADDR,
		GFASTDATA_REST_PATH_NAME,
	)

	REST_URL_NAME_LIST = fmts.ConcatStr(
		GFASTDATA_REST_EXTERNAL_ADDR_LIST,
		GFASTDATA_REST_PATH_NAME_LIST,
	)

	REST_URL_ID = fmts.ConcatStr(
		GFASTDATA_REST_EXTERNAL_ADDR,
		GFASTDATA_REST_PATH_ID,
	)

	handlerEnv = handler.Env{
		GFASTDATA_REST_ADDR: GFASTDATA_REST_ADDR,
		GFASTDATA_GRPC_ADDR: GFASTDATA_GRPC_ADDR,
		REST_URL_NAME:       REST_URL_NAME,
		REST_URL_NAME_LIST:  REST_URL_NAME_LIST,
		REST_URL_ID:         REST_URL_ID,
		GrpcClient:          connect.NewGrpcClient(GFASTDATA_GRPC_ADDR).ConnectClient(),
	}
)

func main() {
	q := quick.New()
	q.Use(logger.New())
	q.Use(msguuid.New())
	q.Use(cors.New(), "cors")
	q.Get("/v1/list/:protocol/hero", handlerEnv.GetHeroList)
	q.Get("/v1/:protocol/hero/:param", handlerEnv.GetHero)
	q.Get("/ping", func(c *quick.Ctx) error {
		c.Set("Content-Type", "application/json")
		c.Set("Engine", "Quick")
		return c.Status(200).SendString("pong")
	})

	if err := q.Listen(GAPI_ADDR); err != nil {
		log.Println("error listening: ", err)
	}
}
